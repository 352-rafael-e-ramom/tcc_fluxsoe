-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09-Nov-2019 às 15:49
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cedup`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `alunos`
--

CREATE TABLE IF NOT EXISTS `alunos` (
  `matricula` varchar(10) NOT NULL,
  `nome_completo` varchar(50) NOT NULL,
  `turma` int(11) NOT NULL,
  `curso` varchar(50) NOT NULL,
  `data_de_nascimento` date NOT NULL,
  PRIMARY KEY (`matricula`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `alunos`
--

INSERT INTO `alunos` (`matricula`, `nome_completo`, `turma`, `curso`, `data_de_nascimento`) VALUES
('4544277719', 'Rafael dos Santos', 352, 'Informatica', '2002-07-10'),
('8458485484', 'Robson', 352, 'Informatica', '2001-08-08'),
('454438422', 'Ramom Goulart Serafim', 353, 'informatica', '2001-10-24'),
('4544291665', 'Vitor Jacinto Frassetto', 353, 'informatica', '2001-09-24'),
('1111111', 'Vinicius frassetto', 354, 'informatica', '2001-09-24'),
('4544270', 'Carlos Henrique', 352, 'info', '2002-04-15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `observacoes`
--

CREATE TABLE IF NOT EXISTS `observacoes` (
  `id_observacao` int(11) NOT NULL AUTO_INCREMENT,
  `matricula` varchar(10) DEFAULT NULL,
  `observacao` varchar(200) NOT NULL,
  `data` date DEFAULT NULL,
  `horario` time DEFAULT NULL,
  PRIMARY KEY (`id_observacao`),
  KEY `matricula` (`matricula`),
  KEY `matricula_2` (`matricula`),
  KEY `matricula_3` (`matricula`),
  KEY `matricula_4` (`matricula`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
