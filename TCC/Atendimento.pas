unit Atendimento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls,
  Vcl.Imaging.jpeg, Vcl.ExtCtrls, Data.DB, Vcl.Grids, Vcl.DBGrids, ppPrnabl,
  ppClass, ppCtrls, ppBands, ppCache, ppDesignLayer, ppParameter, ppDB,
  ppDBPipe, ppComm, ppRelatv, ppProd, ppReport, Vcl.Buttons;

type
  TForm8 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    nome_completo: TDBText;
    turma: TDBText;
    matricula: TDBText;
    curso: TDBText;
    datanasc: TDBText;
    Label6: TLabel;
    observacoes: TDBEdit;
    bt_voltar: TButton;
    DBListBox1: TDBListBox;
    imprimir: TButton;
    Image1: TImage;
    DBGrid1: TDBGrid;
    data: TDBText;
    horario: TDBText;
    ppReport1: TppReport;
    ppDBPipeline1: TppDBPipeline;
    ppDBPipeline2: TppDBPipeline;
    ppParameterList1: TppParameterList;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText1: TppDBText;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    procedure bt_voltarClick(Sender: TObject);
    procedure imprimirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure observacoesClick(Sender: TObject);







  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form8: TForm8;

implementation

{$R *.dfm}

 uses Inicio, DataModule, Registrar, Cadastro, CadastrosBD2;

procedure TForm8.bt_voltarClick(Sender: TObject);
begin
 Form7 := TForm7.Create(Application);
Form8.Hide;
try
  Form7.ShowModal;
finally
  Form7.Free;
end;
end;

procedure TForm8.imprimirClick(Sender: TObject);
begin
//DataModule2.FDQuery2.Append;
DataModule2.FDQuery2.FieldValues['matricula'] := DataModule2.FDQuery1.FieldByName('matricula').AsString;
DataModule2.FDQuery2.FieldValues['data'] := DateToStr(date);
DataModule2.FDQuery2.FieldValues['horario'] := TimeToStr(time);
DataModule2.FDQuery2.Post;

ShowMessage('________________________________'+#13+
' '+ data.Field.AsString +'          '+#13+
' '+ horario.Field.AsString +'       '+#13+
'                                    '+#13+
' '+ nome_completo.Field.AsString +' '+#13+
'                                    '+#13+
'  Motivo da ida ao SOE:                '+#13+
'  '+ observacoes.Field.AsString +'  '+#13+
'                                    '+#13+
'                                    '+#13+
'  AUTORIZAÇÃO                       '+#13+
'________________________________');

//' + DataModule2.FDQuery2.SQL.Add('SELECT nome_completo FROM alunos WHERE nome :PNOME') + '

ppReport1.Print;


end;



procedure TForm8.observacoesClick(Sender: TObject);
begin
         observacoes.Text:='';

end;



procedure TForm8.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate();
end;

procedure TForm8.FormCreate(Sender: TObject);
 var

  Search : string[10];

begin

try

Search :=  datamodule2.fdquery1.fieldbyname('matricula').asstring;

DataModule2.FDQuery2.close;

DataModule2.FDQuery2.SQL.Clear;

DataModule2.FDQuery2.SQL.Add('SELECT * FROM observacoes WHERE matricula =  '+chr(39)+ Search +chr(39));

DataModule2.FDQuery2.Open;

DataModule2.FDQuery2.Append;

except
on e :exception do
ShowMessage(e.ClassName+': '+e.message);
end;

end;

end.
