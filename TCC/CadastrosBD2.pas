unit CadastrosBD2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Imaging.jpeg, Vcl.ExtCtrls,
  Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TForm7 = class(TForm)
    Label1: TLabel;
    DBGrid1: TDBGrid;
    bt_consultar: TButton;
    bt_voltar: TButton;
    Edit1: TEdit;
    Image1: TImage;
    procedure bt_voltarClick(Sender: TObject);
    procedure bt_consultarClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form7: TForm7;

implementation

{$R *.dfm}

 uses Inicio, DataModule, Registrar, Cadastro, Atendimento;

procedure TForm7.bt_consultarClick(Sender: TObject);
begin
Form8 := TForm8.Create(Application);
 Form7.Hide;
try
  Form8.ShowModal;
finally
  Form8.Free;
end;
end;

procedure TForm7.bt_voltarClick(Sender: TObject);
begin
Form1 := TForm1.Create(Application);
Form7.Hide;
try
  Form1.ShowModal;
finally
  Form1.Free;
end;
end;

procedure TForm7.Edit1Change(Sender: TObject);
begin

   DataModule2.FDQuery1.Close;
   DataModule2.FDQuery1.Params.ParamByName('PNOME').Value := Edit1.Text + '%';
   DataModule2.FDQuery1.Open;

end;

procedure TForm7.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate();
end;

end.
