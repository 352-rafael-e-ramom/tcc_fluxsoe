unit CadastrosBD;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.Imaging.jpeg, Vcl.ExtCtrls;

type
  TForm4 = class(TForm)
    DBGrid1: TDBGrid;
    bt_excluir: TButton;
    bt_consultar: TButton;
    Image1: TImage;
    bt_voltar: TButton;
    Label1: TLabel;
    Edit1: TEdit;
    procedure bt_abrirClick(Sender: TObject);
    procedure bt_consultarClick(Sender: TObject);
    procedure bt_excluirClick(Sender: TObject);
    procedure bt_voltarClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

{$R *.dfm}

   uses Inicio, DataModule, Registrar, Cadastro;



procedure TForm4.bt_abrirClick(Sender: TObject);
begin
DataModule2.FDQuery1.Open;
end;

procedure TForm4.bt_consultarClick(Sender: TObject);
begin
Form5 := TForm5.Create(Application);
 Form4.Hide;
try
  Form5.ShowModal;
finally
  Form5.Free;
end;
end;

procedure TForm4.bt_excluirClick(Sender: TObject);
begin
DataModule2.FDQuery1.Delete;
end;

procedure TForm4.bt_voltarClick(Sender: TObject);
begin
Form1 := TForm1.Create(Application);
Form4.Hide;
try
  Form1.ShowModal;
finally
  Form1.Free;
end;
end;

procedure TForm4.Edit1Change(Sender: TObject);
begin

   DataModule2.FDQuery1.Close;
   DataModule2.FDQuery1.Params.ParamByName('PNOME').Value := Edit1.Text + '%';

   //DataModule2.FDQuery1.Filter := 'nome_completo like "' + QuotedStr(Edit1.Text)+'"' ;
   //ShowMessage(DataModule2.FDQuery1.Filter);
   DataModule2.FDQuery1.Open;

   //DataModule2.FDQuery1.SQL.Clear;
   //DataModule2.FDQuery1.SQL.Add('select * from alunos');
   //DataModule2.FDQuery1.SQL.Add('where nome_completo like "'+Edit1.Text+'"');

   //DataModule2.FDQuery1.Open;




end;

procedure TForm4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Application.Terminate();
end;

end.
