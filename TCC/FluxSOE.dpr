program FluxSOE;

uses
  Vcl.Forms,
  Inicio in 'Inicio.pas' {Form1},
  DataModule in 'DataModule.pas' {DataModule2: TDataModule},
  Registrar in 'Registrar.pas' {Form3},
  CadastrosBD in 'CadastrosBD.pas' {Form4},
  Cadastro in 'Cadastro.pas' {Form5},
  EditarCad in 'EditarCad.pas' {Form6},
  CadastrosBD2 in 'CadastrosBD2.pas' {Form7},
  Atendimento in 'Atendimento.pas' {Form8},
  Splash in 'Splash.pas' {Form9};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm9, Form9);
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TDataModule2, DataModule2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TForm6, Form6);
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(TForm8, Form8);
  Application.Run;
end.
